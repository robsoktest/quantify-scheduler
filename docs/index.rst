Welcome to Quantify's documentation!
======================================
..
.. include:: readme.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents
   :glob:

   installation
   user_guide
   tutorials/*
   tutorials/qblox/index
   tutorials/zhinst/index
   contributing
   authors
   changelog
   bibliography

.. toctree::
   :maxdepth: 2
   :caption: API Reference
   :glob:

   autoapi/quantify_scheduler/index

Indices and tables
===================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

